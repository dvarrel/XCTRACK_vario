### USB
serial device - CDC
vendor 16d0 name : digistump.com
product 087e name : digispark serial


### upgrade digispark bootloader to increase free rom to 6650 instead of 6012

https://koen.vervloesem.eu/blog/how-to-upgrade-the-micronucleus-bootloader-on-the-digispark/

#### Build the micronucleus command-line tool:

git clone https://github.com/micronucleus/micronucleus.git
cd micronucleus/commandline
sudo apt install libusb-dev
make
sudo make install

#### The Micronucleus project offers firmware files to upgrade older versions, so then upgrading the existing firmware was as easy as:

cd ..
micronucleus --run firmware/upgrades/upgrade-t85_default.hex (6650 bytes)

micronucleus --run firmware/upgrades/upgrade-t85_aggressive.hex (6780 bytes)


#### Copy the executable to update arduino:

cp micronucleus ~/.arduino15/packages/digistump/tools/micronucleus/2.0a4/

#### upgrade board.txt for bootloader 6650:

nano ~/.arduino15/packages/digistump/hardware/avr/1.6.7/boards.txt

digispark-tiny-upgrade.name=Digispark (Upgrade - 16.5mhz)
digispark-tiny.upload.maximum_size=6650
