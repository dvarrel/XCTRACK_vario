# VARIO XCTRACK VERSION 1.0.4
mcu supported :
* Arduino Nano, pro mini ( usb or bluetooth )
* esp32, bluetooth integrated but consummes a lot (80mA instead of 30mA with Nano)
* experimental : Digispark USB (with bootloader upgrade because of sketch space) 

You have several choices for programming :
* usb mode or bluetooth mode (add bluetooth module for arduino HC06 for example)
* fastest mode or frequency mode
* sentence : VARIO LK8000 or PRS sentence

##default is bluetooth VARIO 12 data/s ( xctrack says 5-10Hz is ok )
* https://gitlab.com/xcontest-public/xctrack-public/-/issues/615

if you use BLUETOOTH_MODE, you do not need USB_MODE : both is just for debugging

### sentences availables for xctrack : 
 LK8000 sentence : 32 chars to send
 "$LK8EX1,pressure,altitude,vario,temperature,battery,*checksum"
 pressure is in Pa (integer)

 VARIO sentence : 19 chars to send
 "$VARIO,pressure,vario*checksum"
 pressure is in hPa (float) and vario in dm/s or empty string.

 PRS sentence : 10 chars to send
 "PRS 16C80"
 PRS pressure in pascal (integer) in hexadecimal format

 bluetooth in 9600 bauds -> 1.25ms for a char \
 LK8000 | VARIO | PRS
 ---|---|---
 40ms   | 24ms  | 12ms

 PRS is faster but LK8000 and VARIO have got checksum !
