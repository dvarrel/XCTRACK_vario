#include "defs.h" // see defs.h for options
VARIO vario(S_VARIO);

void loop(void) {
  static uint8_t n = 0;
#ifndef ESP32
  wdt_reset();
#endif
#if !TEST_MODE
  vario.sensor.readOut(); // with maxi resolution, it takes about 20ms
#else
  delay(20);
#endif
  vario.pressure += vario.sensor.getPres();
  n++;
  static uint32_t endTime ;
  #ifdef ESP32 
  if (vario.ble.isConnected())
  #endif 
  if (millis() >= endTime)
  {
    endTime = millis() + (1000 / FREQUENCY);
    vario.pressure /= n;
#ifdef __AVR_ATtiny85__
    Serial.print("PRS ");
    Serial.print(vario.pressure,HEX);
#else
  vario.format_sentence();
  #ifdef AUTO_CUT_OFF_PERIOD
  static uint16_t nAutoCutOff;
  if (nAutoCutOff++ == AUTO_CUT_OFF_PERIOD){
    for (uint8_t i=0;i<sizeof(pinAutoCutOff);i++){
      digitalWrite(pinAutoCutOff[i], 1);
    }
  }
  if (nAutoCutOff >= (AUTO_CUT_OFF_PERIOD + AUTO_CUT_OFF_DELAY)){
    nAutoCutOff = 0;
    for (uint8_t i=0;i<sizeof(pinAutoCutOff);i++){
      digitalWrite(pinAutoCutOff[i], 0);
    }
  }
  #endif
  static uint16_t nTimeInfos ;
  if (nTimeInfos++ > EXTRA_INFOS_PERIOD){
    nTimeInfos = 0;
    vario.collect_extra_infos();
  }
#endif
#if USB_MODE
  Serial.print(vario.str_out);
#endif
#if BLUETOOTH_MODE
  vario.send_bluetooth();
#endif
    vario.pressure = 0;
    n = 0;
  }
#ifdef ESP32
  vario.ble.update();
#endif
}

void setup() {
  vario.begin();
#ifdef __AVR_ATtiny85__
  Serial.begin();
  Serial.delay(10000);
#endif
#ifdef ESP32
  if (!vario.sensor.begin(pinSDA,pinSCL)) {
#else
  wdt_enable(WDTO_1S);  //enable the watchdog 1s without reset
  if (!vario.sensor.begin()) {
#endif
#if USB_MODE
    vario.sensor.debug();
#endif
#ifdef __AVR_ATtiny85__
    Serial.delay(1200);  //for watchdog timeout
#else
    vario.send_error();
#if !TEST_MODE
    delay(1200);  //for watchdog timeout
#ifdef ESP32
    ESP.restart();
#endif 
#endif
#endif
  }
}


