#include <HardwareSerial.h>
#include <Arduino.h>
#include "defs.h"

VARIO::VARIO(sentence_id s){
  sentence = s;
}

void VARIO::begin()
{
#ifdef pinMS5611Gnd
  pinMode(pinMS5611Gnd,OUTPUT);
  digitalWrite(pinMS5611Gnd,0);
#endif
#ifdef pinMS5611Vcc
  pinMode(pinMS5611Vcc,OUTPUT);
  digitalWrite(pinMS5611Vcc,1);
#endif
#ifdef pinSPPGnd
  pinMode(pinSPPGnd,OUTPUT);
  digitalWrite(pinSPPGnd,0);
#endif
#ifdef AUTO_CUT_OFF_PERIOD
  for (uint8_t i=0;i<sizeof(pinAutoCutOff);i++){
    pinMode(pinAutoCutOff[i],OUTPUT);
    digitalWrite(pinAutoCutOff[i],0);
  }
#endif
#ifdef ESP32
  analogReadResolution(10);
#else
  wdt_disable();
#endif

#if USB_MODE
  Serial.begin(USB_SPEED);
  Serial.println(MESSAGE_VERSION);
  Serial.println(MESSAGE_COMPILE_DATE);
  Serial.println(MESSAGE_CHECK);
#if BLUETOOTH_MODE
  Serial.println(MESSAGE_BLUETOOTH);
#endif
#endif
#if BLUETOOTH_MODE
  #ifdef ESP32
  ble.init();
  #if USB_MODE
  Serial.println(MESSAGE_BLE_ADVERTISING);
  #endif
  #else
  BTserial = new SoftwareSerial(RX,TX);
  BTserial->begin(BLUETOOTH_SPEED);
  BTserial->stopListening();
  BTserial->println(MESSAGE_VERSION);
  BTserial->println(MESSAGE_COMPILE_DATE);
  BTserial->println(MESSAGE_CHECK);
  #endif
#endif
}

#ifndef _AVR_ATtiny85__

void VARIO::format_sentence(){
  if (sentence == S_PRS){
    snprintf(str_out, sizeof(str_out),FORMATS[sentence],(uint32_t)(pressure));
  }else{
    if (sentence == S_VARIO){
      //$VARIO,pressure,vario*ch pressure(float) is in hPa and vario in dm/s or empty string.
      //$VARIO,1013.25,,*45
      pressure = pressure/100.;
      char pstr[12];
      dtostrf(pressure,sizeof(pstr)-1, 2, pstr);
      trimChar(pstr);
      snprintf(str_out,sizeof(str_out),FORMATS[sentence],pstr);
    }else{

      snprintf(str_out,sizeof(str_out),FORMATS[sentence],(uint32_t)(pressure));
    }
    // Calculating checksum for data string without $ caracter
    uint16_t checksum = 0, bi;
    for (uint8_t ai = 1; ai < strlen(str_out); ai++) {
      bi = (uint8_t)str_out[ai];
      checksum ^= bi;
    }
    char s_checksum[128];
    snprintf(s_checksum,sizeof(s_checksum),"*%X\n",checksum);
    strcat(str_out,s_checksum);
  }  
}


void VARIO::trimChar(char * s){
  char ts[128];
  strcpy (ts,s);
  uint8_t i=0;
  while(ts[i]==' ')i++; 
  uint8_t j=0;
  while(ts[i]!='\0'){
    s[j]=ts[i];
    i++;
    j++;
  }
  s[j]='\0';
}

void VARIO::collect_extra_infos(){
  uint8_t soc = measureBattery();
  int8_t temperature = sensor.getTemp();
  snprintf(str_out,sizeof(str_out),FORMAT_XCTOD,temperature,soc);
}

uint8_t VARIO::measureBattery(){
  #if defined(ARDUINO_AVR_NANO) || defined(ARDUINO_AVR_UNO)
  const float Vdec = 0.;
  const float Vref = 5.;
  const float Vdiv = 1.;
  #endif 
  #ifdef ARDUINO_AVR_PRO
  analogReference(INTERNAL);
  const float Vdec = 0.;
  const float Vref = 1.1;
  const float Vdiv = 4.;
  #endif 
  #ifdef ESP32
  const float Vdec = -0.10;
  const float Vref = 3.3;
  const float Vdiv = 2.;
  #endif 

  uint16_t N = 0;
  #define nMes 5
  for (uint8_t i=0;i<nMes;i++){
    N = N + analogRead(pinBattery);
    delay(1);
  } 
  N = N / nMes;
  float Vbat = ((N*Vref/1023.)+Vdec)*Vdiv;
  const float minVbat = 3.3;
  const float maxVbat = 3.95;
  if (Vbat < minVbat) return 0;
  if (Vbat > maxVbat) return 100;
  float SOC = ((Vbat-minVbat)/(maxVbat-minVbat))*100;
  return (uint8_t)SOC;
}

void VARIO::send_error(){
#if USB_MODE
  Serial.println(MESSAGE_ERROR);
  Serial.println(MESSAGE_RESET);
  Serial.flush();
#endif
#if BLUETOOTH_MODE && !ESP32
  BTserial->println(MESSAGE_ERROR);
  BTserial->println(MESSAGE_RESET);
  BTserial->flush();
#endif
}

void VARIO::send_bluetooth(){
#ifdef ESP32
  ble.send(str_out);
#else
#if BLUETOOTH_MODE
  BTserial->print(str_out);
#endif
#endif
}

#endif

