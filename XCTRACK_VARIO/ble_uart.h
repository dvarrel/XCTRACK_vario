#ifndef BLE_UART_h
#define BLE_UART_h
#ifdef ESP32

#define BLE_NAME "XCTRACK_BLE"

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <WiFi.h>
// Nordic UART service UUID
#define SERVICE_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" 
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

class BLE {
  private:
    BLEServer *pServer = NULL;
    BLECharacteristic *pTxCharacteristic;
    
  public:
    void init();
    void update();
    void send(char *str);
    bool isConnected();
};
#endif

#endif
