#ifdef ESP32
#include "ble_uart.h"

bool deviceConnected = false;
bool oldDeviceConnected = false;

class MyServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    deviceConnected = false;
  }
};

class MyCallbacks: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic* pCharacteristic) {
      /*std::string rxValue = pCharacteristic->getValue();
      if (rxValue.length() > 0)
        Serial.println(rxValue.c_str());*/
  }
};


void BLE::init() {
  BLEDevice::init(BLE_NAME);
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_TX,
    BLECharacteristic::PROPERTY_NOTIFY
  );
  pTxCharacteristic->addDescriptor(new BLE2902());
  BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID_RX,
    BLECharacteristic::PROPERTY_WRITE
  );
  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->setMinPreferred(0x06); // iPhone issue
  pAdvertising->setMaxPreferred(0x12); // iPhone issue
  pAdvertising->setScanResponse(false);
  pAdvertising->start();
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

void BLE::update(){
  //disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    oldDeviceConnected = deviceConnected;
  }
}

void BLE::send(char *str){
  uint8_t ascii[50];
  uint8_t i = 0;
  while (str[i] != '\0'){
    ascii[i] = str[i];
    i++;
  }
  pTxCharacteristic->setValue(ascii, i);
  pTxCharacteristic->notify();
}

bool BLE::isConnected(){
  return deviceConnected;
}

#endif
