# XCTRACK_vario DO IT YOURSELF !

<img src="img/xctrack.webp" width="512px"/>
<img src="img/xctrack_powerbank.webp" width="256px"/>

## Table of Contents

- [Overview](#overview)
- [Components](#components)
- [Step by step](#step-by-step)
- [Diagrams](#diagrams)
    * [USB support only](#usb-support-only)
    * [USB and bluetooth support](#usb-and-bluetooth-support)
- [Known issues](#known-issues)
  
## Overview 

Variometer for [XCTRACK](https://xctrack.org/) based on MS5611 (high precision pressure and temperature sensor) and VARIO, LK8000 or PRS sentence.

It works via USB or Bluetooth on XCtrack

Power consumption ~0.1W ( ~0.15W with bluetooth)

### Components
- Arduino Nano Pro-Mini , ESP32, or Digispark(experimental)
- MS5611 pressure and temperature sensor
- serial bluetooth module in option for nano/pro (jdy30/31 hc-05 or hc-06 like)
- USB mini/micro/type c OTG cable (if USB mode), depends on your phone
<a href="https://www.aliexpress.com/p/wishlist/shareReflux.html?groupId=97VZ%2FinWsMgTINCTGJmjmPX9vt1FdJRj1UhXbASF0xQ%3D" target="_blank">Aliexpress</a>

- case :
  - powerbank with on/off button <a href="https://fr.aliexpress.com/item/1005005981563008.html" target="_blank">Aliexpress</a>
  - PLA box 3D printing for Arduino Nano
<a href="https://cad.onshape.com/documents/8ec970df0f91cd7f4dafb0b8/w/81f5e80ab2d84901aa8e29be/e/2df0103f0eb100d6330b3609" target="_blank">Onshape</a>
 - STL files also available for printing in stl folder
  
## Step by step

1) If you want to use it as only usb, just upload the code in XCTRACK_VARIO folder.

1) you have to weld the ms5611 with the nano or digisaprk pcb. Only 4 connections are needed.

Note: To build this vario, you *may* be able to use other Arduinos besides the nano: simply weld the SCL/SDA of the MS5611 to the respective SCL/SDA ports of your Arduino.

## Diagrams

<img src="img/vario_MS5611_bb.webp" height=300/>

### USB support only

<img src="img/nano_usb-c_welded.webp" height=200/>
<img src="img/digispark_welded1.webp" height=200/>
<img src="img/digispark_welded2.webp" height=200/>

### USB and bluetooth support

<img src="img/nano_bluetooth_welded.webp" height=200/>
<img src="img/bluetooth_battery_18650.webp" height=200/>
<img src="img/xctrack_tpu_powerbank.webp" height=200/>
<img src="img/xctrack_esp32c3_powerbank.webp" height=200/>

* with this kind of battery ( 2000mA.h ) life time is about 50h before charging

### Known issues

#### The arduino nano seems bricked
You may have connected the SCL/SDA ports wrong. 

If you can't upload to your arduino:

1) Disconnect it from the computer 
   
1) Press and hold the reset button

1) Plug it back (don't release the reset)
   
1) Click upload (don't release the reset)
   
1) When the IDE says that is uploading (after compiling ends) release the reset button

#### Can't upload
Assuming you are using a nano, you may have an older bootloader version. 

Change the processor to "ATmega328P (Old Bootloader)" 
<img src="img/old-bootloader.webp"/>

#### comparison several sensors sensitivity

<img src="img/SensorData.webp"/>
